<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    
    public function register(Request $request)
    {
        $input = [
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'c_password' => $request->input('c_password')
        ];

        $validator = Validator::make($input, [
            'username' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            $message = $validator->messages();
            return ['message' => $message];
        }

        $input['password'] = bcrypt($request->input('password'));

        $user = User::create($input);
        $token = $user->createToken('Access Token')->accessToken;

        $message = 'User created successfully.';
        $data = [
            'user' => $user,
            'token' => $token
        ];

        return ['message' => $data];
    }

}
