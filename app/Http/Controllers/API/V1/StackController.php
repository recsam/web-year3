<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Stack;
use Illuminate\Http\Request;

class StackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = Request()->query('limit', 0);
        $page = Request()->query('page', 0);

        if (Request()->has('user')) {
            $user_id = Request()->query('user');

            if ($limit > 0 && $page > 0) {
                $stacks = Stack::where('created_by', $user_id)->paginate($limit);
            } else if ($limit > 0) {
                $stacks = Stack::where('created_by', $user_id)->take($limit)->get();
            } else {
                $stacks = Stack::where('created_by', $user_id)->get();
            }
        } else {

            if ($limit > 0 && $page > 0) {
                $stacks = Stack::paginate($limit);
            } else if ($limit > 0) {
                $stacks = Stack::take($limit)->get();
            } else {
                $stacks = Stack::all();
            }
        }

        return $stacks;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'user_id' => 'required'
        ]);

        $stack = new Stack();

        $stack->name = $request->name;
        $stack->created_by = $request->user_id;

        $stack->save();
        return $stack;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
