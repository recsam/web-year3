<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {
        return redirect()->route('admin.users.index');
        return view('admin.dashboard.index');
    }
}
