<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CardController extends Controller
{

    public function create()
    {
        return view('member.cards.create.index');
    }

    public function store(Request $request)
    {
        $validated_data = $request->validate([
            'stacks' => 'required',
            'front_card' => 'required',
            'back_card' => 'required'
        ]);
        return $validated_data;

        $front_card_path = $request->file('front_card')->store('images');
        $back_card_path = $request->file('back_card')->store('images');

        $user_id = Auth::user()->id;

        $card = new Card();
        $card->created_by = $user_id;
        $card->card_front = $front_card_path;
        $card->card_back = $back_card_path;
        $card->save();

        $card->stacks()->attach($request->stacks);
            
        return Redirect('/');
    }

    public function update(Request $request)
    {

    }

    public function delete($id)
    {
    }

    public function forceDelete($id)
    {
    }
}
