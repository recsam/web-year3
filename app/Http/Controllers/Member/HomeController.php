<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Card;
use App\Models\Stack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $recent_cards = Card::latest()->limit(12)->get();
        return view('member.home.index')
            ->with('recent_cards', $recent_cards);
    }
    
}
