<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    function index($id)
    {
        $user = Auth::user();
        $user_profile = $user->profile;
        return view('profile.index')
            ->with('user', $user)
            ->with('user_profile', $user_profile);
    }
}
