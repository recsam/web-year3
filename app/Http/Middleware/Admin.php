<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->auth = auth()->user()
            ? (auth()->user()->role === 'admin')
            : false;

        if ($this->auth !== true)
            return redirect()->route('login')->with('error', 'Access denied. Login to continue!');

        return $next($request);
    }
}
