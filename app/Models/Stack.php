<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stack extends Model
{
    public function cards()
    {
        return $this->belongsToMany('App\Models\Card')->orderBy('updated_at', 'desc');
    }

    public function latest_cards()
    {
        return $this->belongsToMany('App\Models\Card')->orderBy('updated_at', 'asc');
    }
}
