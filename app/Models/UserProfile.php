<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{

    /**
     * Get the user record asscociated with the profile
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
