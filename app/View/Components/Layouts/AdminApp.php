<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class AdminApp extends Component
{
    public $page_key;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($pageKey)
    {
        $this->page_key = $pageKey;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.layouts.admin-app');
    }
}
