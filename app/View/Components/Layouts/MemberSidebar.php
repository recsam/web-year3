<?php

namespace App\View\Components\Layouts;

use App\Models\Stack;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class MemberSidebar extends Component
{

    public $page_key;

    public $sidebar_stacks;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($pageKey)
    {
        $this->page_key = $pageKey;

        $user_id = Auth::user()->id;
        $sidebar_stacks = Stack::where('created_by', $user_id)->get();
        $this->sidebar_stacks = $sidebar_stacks;
        
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.layouts.member-sidebar');
    }
}
