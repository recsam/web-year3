<?php

namespace App\View\Components\Shared;

use Illuminate\View\Component;

class CardList extends Component
{
    
    public $cards;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($cards)
    {
        $this->cards = $cards;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.shared.card-list');
    }
}
