<x-layouts.sidebar-app class="ligher-bg">
    <x-slot name="sidebar">
        <x-layouts.admin-sidebar />
    </x-slot>

    <div id="page-content-wrapper">
        <div class="container-fluid mt-1">
            {{ $slot }}
        </div>
    </div>

</x-layouts.sidebar-app>
