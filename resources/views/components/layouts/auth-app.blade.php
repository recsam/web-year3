<x-layouts.app>
    <div class="py-4">
        {{ $slot }}
    </div>
</x-layouts.app>
