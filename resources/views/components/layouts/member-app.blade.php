<x-layouts.sidebar-app>
    <x-slot name="sidebar">
    <x-layouts.member-sidebar :page-key="$page_key">
        @if($page_key == 'STACK_DETAILS')
        <x-slot name="current_stack">{{ $current_stack }}</x-slot>
        @endif
    </x-layouts.member-sidebar>
    </x-slot>

    <div id="page-content-wrapper" class="mt-1">
        <div class="container">
            {{ $slot }}
        </div>
    </div>

</div>

<x-modals.create-stack />

@push('right-actions')
<form class="form-inline">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
</form>
<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ Auth::user()->username }} <span class="caret"></span>
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('profile', Auth::user()->id) }}">
            My Profile
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>
@endpush

</x-layouts.sidebar-app>
