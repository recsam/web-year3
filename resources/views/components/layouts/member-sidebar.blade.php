<x-layouts.sidebar>
    <div class="list-group list-group-flush mb-4 mt-1">
        <a href="{{ route('home') }}" class="list-group-item list-group-item-action bg-transparent @if($page_key == 'HOME') active @endif" @if($page_key=='HOME') onclick="return false;" @endif>Home</a>
        <a href="#" class="list-group-item list-group-item-action bg-transparent">Explore</a>
        <a href="#" class="list-group-item list-group-item-action bg-transparent">Statistics</a>
    </div>
    <p class="mb-2 text-secondary">Your Cards</p>
    <div class="list-group list-group-flush" id="stacksList">
        @foreach($sidebar_stacks as $sidebar_stack)
        @if($page_key == 'STACK_DETAILS')
        @if(''.$sidebar_stack->id == $current_stack)
        <a href="" onclick="return false;" class="list-group-item list-group-item-action bg-transparent active">{{ $sidebar_stack->name }}</a>
        @else
        <a href="{{ route('stacks.show', $sidebar_stack->id) }}" class="list-group-item list-group-item-action bg-transparent">{{ $sidebar_stack->name }}</a>
        @endif
        @else
        <a href="{{ route('stacks.show', $sidebar_stack->id) }}" class="list-group-item list-group-item-action bg-transparent">{{ $sidebar_stack->name }}</a>
        @endif
        @endforeach
        <div style="cursor: pointer;" class="list-group-item list-group-item-action text-secondary bg-transparent" data-toggle="modal" data-target="#createStackModal">+ Create Stack</div>
    </div>
</x-layouts.sidebar>
