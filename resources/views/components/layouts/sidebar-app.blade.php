<x-layouts.app>
    <div class="d-flex" id="wrapper">
        @if(isset($sidebar)) {{ $sidebar }} @endif
        {{ $slot }}
    </div>
</x-layouts.app>
