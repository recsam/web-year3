<div id="sidebar-wrapper">
    {{ $slot }}
</div>

@push('left-actions')
<button class="navbar-toggler d-inline d-md-none" id="menu-toggle">
    <span class="navbar-toggler-icon"></span>
</button>
@endpush

@push('scripts')
<script>
    $(document).ready(function () {
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    })
</script>
@endpush
