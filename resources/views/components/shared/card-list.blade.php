<div class="card-list">
    <x-shared.card-add />
    @foreach($cards as $card)
    <x-shared.card>
        <img style="object-fit: contain;" class="w-100 h-100" id="previewBackCard" src="{{ asset( 'storage/' . $card->card_front ) }}">
    </x-shared.card>
    @endforeach
</div>
