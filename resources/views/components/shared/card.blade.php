<a {{ $attributes->merge(['class' => 'flash-card bg-white mb-4 d-flex justify-content-center align-items-center'] ) }} style="text-decoration: none;">
    {{ $slot }}
</a>
