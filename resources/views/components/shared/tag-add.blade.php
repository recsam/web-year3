<div class="dropdown">
    <x-shared.tag class="tag-faded" data-toggle="dropdown">
        <i class="fa fa-plus fa-sm"></i> Add Stack
    </x-shared.tag>
    <div class="dropdown-menu" style="cursor: pointer;">
        @foreach($stacks as $stack)
        <div class="dropdown-item" data-id="{{ $stack->id }}">{{ $stack->name }}</div>
        @endforeach
    </div>
</div>

@push('scripts')
<script>
    let addedStacks = [];

    $(document).ready(function () {
        $(document).on('click', '.dropdown-item', function () {
            let id = $(this).data('id');
            for (let i = 0; i < addedStacks.length; i++) {
                if (addedStacks[i] == id) return;
            }
            let text = $(this).text();
            addedStacks.push(id);
            $(`<x-shared.tag>${text}</x-shared.tag>`).insertBefore('#stackList > div:last');
            $("<input />").attr("type", "hidden").attr("name", "stacks[]").val(id).appendTo('#stackListInput');
        })
    })
</script>
@endpush
