<p {{ $attributes->merge(['class' => 'tag']) }}>
    {{ $slot }}
</p>
