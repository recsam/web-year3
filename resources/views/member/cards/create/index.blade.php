<x-layouts.member-app page-key="CREATE_CARDS">
    <h1 class="mb-4">Create Card</h1>
    <form method="POST" action="{{ route('cards.store') }}" enctype="multipart/form-data">
        @csrf
        <h3>Stacks</h3>
        @error('stacks')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="d-flex flex-wrap mb-4" id="stackList">
            <x-shared.tag-add />
        </div>
        <div id="stackListInput"></div>
        <div class="mb-4 mr-sm-4 card-preview-wrapper">
            <h3>Front</h3>
            <div class="card-preview">
                <img class="w-100 h-100" id="previewFrontCard">
            </div>
            <div class="custom-file">
                <input name="front_card" type="file" class="custom-file-input" id="inputFrontCard">
                <label class="custom-file-label text-truncate" for="inputFrontCard">Choose file</label>
            </div>
            @error('front_card')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        </div>
        <div class="mb-4 card-preview-wrapper">
            <h3>Back</h3>
            <div class="card-preview">
                <img class="w-100 h-100" id="previewBackCard">
            </div>
            <div class="custom-file">
                <input name="back_card" type="file" class="custom-file-input" id="inputBackCard">
                <label class="custom-file-label text-truncate" for="inputBackCard">Choose file</label>
            </div>
            @error('back_card')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        </div>
        <div class="d-flex">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary ml-2">Create</button>
        </div>
    </form>

    @push('scripts')
    <script>
        $(document).ready(function () {

            $('#inputFrontCard').change(function () {
                if (this.files && this.files[0]) {
                    let reader =  new FileReader();

                    reader.onload = function(e) {
                        $('#previewFrontCard').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);

                    $(this).next('.custom-file-label').html($(this).val());

                }
            })

            $('#inputBackCard').change(function () {
                if (this.files && this.files[0]) {
                    let reader =  new FileReader();

                    reader.onload = function(e) {
                        $('#previewBackCard').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(this.files[0]);

                    $(this).next('.custom-file-label').html($(this).val());

                }
            })

        })



    </script>
    @endpush

</x-layouts.member-app>
