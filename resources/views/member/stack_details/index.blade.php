<x-layouts.member-app page-key="STACK_DETAILS">
    <x-slot name="current_stack">
        {{ $stack->id }}
    </x-slot>

    <div class="d-flex justify-content-between"> 
        <div>
            <h1 class="d-inline">{{ $stack->name }}</h1>
            <h4 class="d-inline text-secondary ml-2">{{ $cards->count() }} cards</h4>
        </div>
        <div class="dropdown dropleft">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sort by
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{ route('stacks.show', $stack->id) . '?latest=true' }}">Latest</a>
                <a class="dropdown-item" href="{{ route('stacks.show', $stack->id) . '?latest=false' }}">Earliest</a>
            </div>
        </div>
    </div>

    <x-shared.card-list :cards="$cards" />
    {{ $cards->links() }}

</x-layouts.member-app>