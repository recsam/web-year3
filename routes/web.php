<?php

use App\Models\Stack;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (!Auth::guest()) {
        return redirect()->route('dashbaord');
    }
    return view('welcome');
});

Auth::routes();

Route::group([
    'prefix' => '/u',
    'namespace' => 'User',
    'middleware' => 'auth'
], function () {

    Route::get('/{id}', 'ProfileController@index')->name('profile');

});

Route::group([
    'prefix' => '',
    'namespace' => 'Member',
    'middleware' => 'auth'
], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group([
        'prefix' => '/c',
        'as' => 'cards.'
    ], function () {
        Route::post('/', 'CardController@store')->name('store');
        Route::get('/create', 'CardController@create')->name('create');
    });

    Route::group([
        'prefix' => '/s',
        'as' => 'stacks.'
    ], function () {
        Route::post('', 'StackController@store')->name('store');
        Route::get('/{id}', 'Stackcontroller@show')->name('show');
    });


});




Route::group([
    'prefix' => '/admin',
    'namespace' => 'Admin',
    'middleware' => 'admin'
], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashbaord');

    Route::group([
        'as' => 'admin.users.'
    ], function() {
        Route::get('/users', 'UserController@index')->name('index');
    });
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

